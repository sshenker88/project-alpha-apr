from django.contrib import admin

# Register your models here.

from django.contrib import admin
from rust.models import GeneImage

# Register your models here.


@admin.register(GeneImage)
class GeneImageAdmin(admin.ModelAdmin):
    list_display = (
        "gene_let",
        "id",
    )
