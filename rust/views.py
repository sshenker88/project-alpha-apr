from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Clone, GeneImage
import io
import cv2
import numpy as np
from mss import mss
# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision_v1 import types
# Create your views here.


# @login_required
def clone_list(request):
    my_clones_obj = Clone.objects.all()
    # my_clones = Clone.objects.filter(assignee=request.user)
    geneimage = GeneImage.objects.all()
    image_dict = {
    "X": geneimage[0].gene_let,
    "W": geneimage[1].gene_let,
    "H": geneimage[2].gene_let,
    "G": geneimage[3].gene_let,
    "Y": geneimage[4].gene_let,
    }
    my_clones= []
    for clone in my_clones_obj:
        temp = [
            image_dict.get(clone.chrom_1),
            image_dict.get(clone.chrom_2),
            image_dict.get(clone.chrom_3),
            image_dict.get(clone.chrom_4),
            image_dict.get(clone.chrom_5),
            image_dict.get(clone.chrom_6),
        ]
        my_clones.append(temp)
    context = {
        "my_clones": my_clones,
        "clone_objects" : my_clones_obj,
    }
    return render(request, "rust/mine.html", context)




# export GOOGLE_APPLICATION_CREDENTIALS=kyourcredentials.json



# from google.cloud.vision import types
# Instantiates a client
client = vision.ImageAnnotatorClient()
def detect_text(path):
    """Detects text in the file."""
    with io.open(path, 'rb') as image_file:
        content = image_file.read()
    image = types.Image(content=content)
    response = client.text_detection(image=image)
    texts = response.text_annotations
    string = ''
    for text in texts:
        string+=' ' + text.description
    return string




def add_clones(request):
    bounding_box = {'top': 60, 'left': 1520, 'width': 800, 'height': 400}
    sct = mss()
    words_seen = []
    while True:
        sct_img = sct.grab(bounding_box)
        convert = np.array(sct_img)
        cv2.imshow('screen', convert)
        file = 'live.png'
        cv2.imwrite(file, convert)
        detected = detect_text(file)
        if detected:
            species_genes = parse_text(detected)
            if species_genes and species_genes not in words_seen:
                words_seen.append(species_genes)
                print(species_genes[0] + " :" + species_genes[1])
                genes = species_genes[1]
                clone = Clone.objects.create(
                    species=species_genes[0],
                    chrom_1=genes[0], chrom_2=genes[1],
                    chrom_3=genes[2], chrom_4=genes[3],
                    chrom_5=genes[4], chrom_6=genes[5],
                )
                clone.save()

        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            cv2.destroyAllWindows()
            break
    return redirect("clone_list")

def parse_text(raw):
    r_up = raw.upper()
    slice_str = ""
    output = ""
    allowed = "GYHWX"
    valid = False
    if "CORN" in r_up:
        output += "Corn: "
    elif "PUMPKIN" in r_up:
        output += "PUMP: "
    if "GENETICS" in r_up and len(output) == 6:
        gene_ind = r_up.rindex("GENETICS")
        slice_str = r_up[gene_ind + 8:]
        for char in slice_str:
            if char in allowed:
                output += char
            if len(output) == 12:
                valid = True
                break
    if valid:
        species = output[0:4]
        genes = output[6:12]
        both = [species, genes]
        return both
