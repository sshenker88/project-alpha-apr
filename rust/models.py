from django.db import models
from django.conf import settings
# Create your models here.


class Clone(models.Model):
    species = models.CharField(max_length=20)
    chrom_1 = models.CharField(max_length=1)
    chrom_2 = models.CharField(max_length=1)
    chrom_3 = models.CharField(max_length=1)
    chrom_4 = models.CharField(max_length=1)
    chrom_5 = models.CharField(max_length=1)
    chrom_6 = models.CharField(max_length=1)


    # assignee = models.ForeignKey(
    #     settings.AUTH_USER_MODEL,
    #     related_name="clones",
    #     on_delete=models.CASCADE,
    #     null=True,
    #)

class GeneImage(models.Model):
    gene_let = models.ImageField()
    # W = models.ImageField()
    # H = models.ImageField()
    # G = models.ImageField()
    # Y = models.ImageField()
    # gene = models.ForeignKey(
    #     on_delete=models.CASCADE,
    #     related_name="image",
    # )
