from django.urls import path
from .views import clone_list, add_clones

urlpatterns = [
    # path("create/", create_task, name="create_task"),
    path("clones/", clone_list, name="clone_list"),
    path("add/", add_clones, name = "add_clones")
]
