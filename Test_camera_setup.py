#!/usr/bin/env python
# $ chmod +x Test_camera_setup.py
# export GOOGLE_APPLICATION_CREDENTIALS=kyourcredentials.json

import io
import cv2
import numpy as np
from mss import mss
# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision_v1 import types
from rust.models import Clone

# from google.cloud.vision import types
# Instantiates a client
client = vision.ImageAnnotatorClient()
def detect_text(path):
    """Detects text in the file."""
    with io.open(path, 'rb') as image_file:
        content = image_file.read()
    image = types.Image(content=content)
    response = client.text_detection(image=image)
    texts = response.text_annotations
    string = ''
    for text in texts:
        string+=' ' + text.description
    return string

def parse_text(raw):
    r_low = raw.upper()
    slice_str = ""
    output = ""
    allowed = "GYHWX"
    valid = False
    if "CORN" in r_low:
        output += "Corn: "
    elif "PUMPKIN" in r_low:
        output += "PUMP: "
    if "GENETICS" in r_low:
        gene_ind = r_low.rindex("GENETICS")
        slice_str = r_low[gene_ind + 8:]
        # print("length:", r_len,"r_low:", r_low, "gene_ind:", gene_ind)
        for char in slice_str:
            if char in allowed:
                output += char
            if len(output) == 12:
                valid = True
                break

    if valid:
        species = output[0:4]
        genes = output[6:12]
        both = [species, genes]
        return both


def add_clones():

    bounding_box = {'top': 60, 'left': 1520, 'width': 800, 'height': 400}

    sct = mss()
    words_seen = []

    while True:
        sct_img = sct.grab(bounding_box)
        convert = np.array(sct_img)
        cv2.imshow('screen', convert)

        file = 'live.png'
        cv2.imwrite( file, convert)

        detected = detect_text(file)
        if detected:
            species_genes = parse_text(detected)

            if species_genes and species_genes not in words_seen:
            # if True:
                words_seen.append(species_genes)
                print(species_genes[0] + " :" + species_genes[1])
                genes = species_genes[1]
                genes = genes.split()
                Clone.objects.create(
                    species=species_genes[0],
                    chrom_1=genes[0],
                    chrom_2=genes[1],
                    chrom_3=genes[2],
                    chrom_4=genes[3],
                    chrom_5=genes[4],
                    chrom_6=genes[5],
                )



        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            cv2.destroyAllWindows()
            break


add_clones()




# while(True):
#     img = ImageGrab.grab(bbox=(100,10,400,780)) #bbox specifies specific region (bbox= x,y,width,height)
#     img_np = numpy.array(img)
#     frame = cv2.cvtColor(img_np, cv2.COLOR_BGR2GRAY)
#     cv2.imshow("test", frame)
#     # cv2.waitKey(0)
# cv2.destroyAllWindows()



# print("0",cv2.VideoCapture(0))
# print("1",cv2.VideoCapture(1))

# print(cv2.VideoCapture(0))
# print(cv2.VideoCapture(1))
# print(cv2.VideoCapture(2))
# print(cv2.VideoCapture(3))
# print(cv2.VideoCapture(4))
# print(cv2.VideoCapture(5))
# print(cv2.VideoCapture(6))


# cap = cv2.VideoCapture(0)
# while(True):
#     # Capture frame-by-frame
#     ret, frame = cap.read()
#     file = 'live.png'
#     cv2.imwrite( file,frame)
#     # print OCR text
#     print(detect_text(file))
#     # Display the resulting frame
#     cv2.imshow('frame',frame)
# # When everything done, release the capture
# cap.release()
# cv2.destroyAllWindows()
