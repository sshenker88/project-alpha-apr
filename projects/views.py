from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm

# Create your views here.


@login_required
def project_list(request):
    my_projects = Project.objects.filter(owner=request.user)
    context = {"my_projects": my_projects}
    return render(request, "project_list.html", context)


@login_required
def show_project(request, id=id):
    project_details = Project.objects.get(id=id)
    context = {
        "project": project_details,
    }
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
