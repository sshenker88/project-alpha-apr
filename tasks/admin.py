from django.contrib import admin
from tasks.models import Task
from rust.models import Clone

# Register your models here.


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    )

@admin.register(Clone)
class CloneAdmin(admin.ModelAdmin):
    list_display = (
        "species",
        "chrom_1",
        "chrom_2",
        "chrom_3",
        "chrom_4",
        "chrom_5",
        "chrom_6",
    )
